import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NodeTest {

    @Test
    public void generateNodeTest() {
        Node fruit = new Node("fruta", null, null);
        Node node = new Node("Maçã", null, null);

        fruit.setRight(node);

        Node.generateNode(node, "Melancia", "verde");

        assertEquals("Unexpected node!", "verde", node.getNodeName());
        assertEquals("Unexpected node!", "Maçã", node.getLeft().getNodeName());
        assertEquals("Unexpected node!", "Melancia", node.getRight().getNodeName());
    }

}