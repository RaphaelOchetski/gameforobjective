import javax.swing.JOptionPane;

import static java.text.MessageFormat.format;
import static java.util.Objects.nonNull;

public class Game {

    private static final String QUESTION = "O prato que você pensou é {0}?";
    private static final String MAIN_TITLE = "Jogo Gourmet";

    static Node bolo = new Node("Bolo de Chocolate", null, null);
    static Node lasanha = new Node("Lasanha", null, null);
    static Node rootMassa = new Node("massa", null, null);

    public static void main(String[] args) {
        setupTree();
        startGame();
    }

    private static void setupTree() {
        rootMassa.setRight(lasanha);
        rootMassa.setLeft(bolo);
    }

    private static void startGame() {
        JOptionPane.showMessageDialog(null, "Pense em um prato que gosta", MAIN_TITLE, JOptionPane.INFORMATION_MESSAGE);
        startQuestions(rootMassa);
    }


    private static void startQuestions(Node node) {
        if(nonNull(node.getRight())){
            int reply = getReply(node);
            if(reply == JOptionPane.YES_OPTION) {
                startQuestions(node.getRight());
            }
            else if (reply == JOptionPane.NO_OPTION){
                startQuestions(node.getLeft());
            }
            System.exit(0);
        }

        int reply = getReply(node);

        if (reply == JOptionPane.YES_OPTION) {
            JOptionPane.showMessageDialog(null, "Acertei de novo!", MAIN_TITLE, JOptionPane.INFORMATION_MESSAGE);
            startGame();
        }

        addNode(node);

    }

    private static int getReply(Node node) {
        return JOptionPane.showConfirmDialog(null, format(QUESTION, node.getNodeName()), "Confirm", JOptionPane.YES_NO_OPTION);
    }

    private static void addNode(Node node) {
        String food = JOptionPane.showInputDialog(null, "Qual prato você pensou?", "Desisto", JOptionPane.QUESTION_MESSAGE);
        String feature = JOptionPane.showInputDialog(null, food + " é ________ mas " + node.getNodeName() + " não.", "Complete", JOptionPane.QUESTION_MESSAGE);
        Node.generateNode(node, food, feature);
        startGame();
    }

}
