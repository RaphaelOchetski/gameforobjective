public class Node {

    private String nodeName;

    private Node left;

    private Node right;

    public Node(String nodeName, Node left, Node right) {
        this.nodeName = nodeName;
        this.left = left;
        this.right = right;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public static void generateNode(Node node, String newFood, String newFeature){

        Node aux = new Node(node.getNodeName(), null, null);

        Node nodeFood = new Node(newFood, null, null);
        node.setRight(nodeFood);
        node.setNodeName(newFeature);
        node.setLeft(aux);
    }
}
